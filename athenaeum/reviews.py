import getpass, hashlib, json, functools

from PyQt5.QtCore import QObject, pyqtProperty, pyqtSignal, QUrl, QByteArray, pyqtSlot, QLocale, QSysInfo
from PyQt5.QtNetwork import QNetworkRequest

class ReviewsProvider(QObject):
    reviewChanged = pyqtSignal()
    API_URI = 'https://odrs.gnome.org/1.0/reviews/api/'

    def __init__(self, networkAccessManager=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._networkAccessManager = networkAccessManager
        # self._networkAccessManager.finished.connect(self.storeResponse)
        self._userId = self.generateUserId()
        self._gameReviews = {}

    def getUsername(self):
        # changing from os.getLogin(), which fails under flatpak on ubuntu, to 
        # getpass.getuser(), per advice here: https://bugs.python.org/issue40821
        return getpass.getuser()

    def getMachineId(self):
        with open('/etc/machine-id') as machineIdFile:
            for machineId in machineIdFile:
                return machineId.rstrip()

    def generateUserId(self):
        salted = 'athenaeum['+ self.getUsername() +':'+ self.getMachineId() +']'
        hash_object = hashlib.sha1(salted.encode("utf-8"))
        return hash_object.hexdigest()

    @pyqtSlot(result=str)
    def getUserId(self):
        return self._userId

    @pyqtSlot(str, result='QJsonObject')
    def getFetchParams(self, gameId, limit=5):
        return {
            'app_id': gameId, 
            'limit': limit, 
            'user_hash': self.getUserId(), 
            'locale': QLocale.system().name(), 
            'distro': QSysInfo.productType(), 
            'version': 'latest'
        }


    # @pyqtSlot(str, result=obj)
    # def getReviewsForGameId(self, gameId):
    #     if (gameId in self._gameReviews.keys()):
    #         print("k")
    #     else:
    #         print("ok")

    # @filterValue.setter
    # def filterValue(self, filterValue):
    #     #if filterValue != self._filterValue:
    #     self._filterValue = filterValue
    #     self._metaRepository.set(key='filter', value=self.filterValue)
    #     self.paramsChanged.emit()

    # def load(self, gameId=None, limit=5):
    #     request = QNetworkRequest(QUrl(self.API_URI + 'fetch'))
    #     request.setHeader(QNetworkRequest.ContentTypeHeader, "application/json")
    #     body = {
    #         'app_id': gameId, 
    #         'limit': limit, 
    #         'user_hash': self._userId, 
    #         'locale': 'en', 
    #         'distro': 'fedora', 
    #         'version': 'latest'
    #     }

    #     self._networkAccessManager.post(request, bytearray(json.dumps(body), 'utf-8'))

    # def storeResponse(self, reply):
    #     reviews = json.loads(bytearray(reply.readAll()).decode("utf-8"))
    #     reviews = list(filter(lambda review: 'rating' in review.keys(), reviews))
    #     if (len(reviews)):
    #         gameId = reviews[0]['app_id']
    #         self._gameReviews[gameId] = reviews
