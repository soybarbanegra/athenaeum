import json, sys
from abc import ABC, ABCMeta, abstractmethod

from PyQt5.QtCore import QObject, pyqtProperty, pyqtSignal, pyqtSlot, QUrl, QThreadPool, QRunnable, QByteArray
from PyQt5.QtQml import QQmlListProperty
from PyQt5.QtNetwork import QNetworkRequest
from PyQt5.QtXml import QDomDocument

from workers import Worker

class GameServerMeta(type(QObject), ABCMeta):
    pass

class GameServer(QObject, metaclass=GameServerMeta):
    gameIdChanged = pyqtSignal()
    addressChanged = pyqtSignal()
    playersChanged = pyqtSignal()
    maxPlayersChanged = pyqtSignal()
    mapChanged = pyqtSignal()
    countryChanged = pyqtSignal()
    nameChanged = pyqtSignal()

    def __init__(self, gameId=None, address=None, players=None, maxPlayers=None, map=None, country=None, name=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._gameId = gameId
        self._address = address
        self._players = players
        self._maxPlayers = maxPlayers
        self._map = map
        self._country = country
        self._name = name

    @pyqtProperty('QString', notify=gameIdChanged)
    def gameId(self):
        return self._gameId

    @gameId.setter
    def gameId(self, gameId):
        if gameId != self._gameId:
            self._gameId = gameId
            self.gameIdChanged.emit()

    @pyqtProperty('QString', notify=addressChanged)
    def address(self):
        return self._address

    @address.setter
    def address(self, address):
        if address != self._address:
            self._address = address
            self.addressChanged.emit()

    @pyqtProperty('QString', notify=playersChanged)
    def players(self):
        return self._players

    @players.setter
    def players(self, players):
        if players != self._players:
            self._players = players
            self.playersChanged.emit()

    @pyqtProperty('QString', notify=maxPlayersChanged)
    def maxPlayers(self):
        return self._maxPlayers

    @maxPlayers.setter
    def maxPlayers(self, maxPlayers):
        if maxPlayers != self._maxPlayers:
            self._maxPlayers = maxPlayers
            self.maxPlayersChanged.emit()

    @pyqtProperty('QString', notify=mapChanged)
    def map(self):
        return self._map

    @map.setter
    def map(self, map):
        if map != self._map:
            self._map = map
            self.mapChanged.emit()

    @pyqtProperty('QString', notify=countryChanged)
    def country(self):
        return self._country

    @country.setter
    def country(self, country):
        if country != self._country:
            self._country = country
            self.countryChanged.emit()

    @pyqtProperty('QString', notify=nameChanged)
    def name(self):
        return self._name

    @name.setter
    def name(self, name):
        if name != self._name:
            self._name = name
            self.nameChanged.emit()

    @abstractmethod
    def launchArgs(self):
        pass

class MinetestServer(GameServer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, gameId='net.minetest.Minetest', **kwargs)

    def launchArgs(self):
        parts = self._address.split(':')
        return '--address {address} --port {port} --go'.format(address=parts[0], port=parts[1])

class RedEclipseServer(GameServer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, gameId='net.redeclipse.RedEclipse', **kwargs)

    def launchArgs(self):
        return '-x"connect ' + ' '.join(self._address.split(':')) + '"'

class SuperTuxKartServer(GameServer):
    def __init__(self, id=None, *args, **kwargs):
        super().__init__(*args, gameId='net.supertuxkart.SuperTuxKart', **kwargs)
        self._id = id

    def launchArgs(self):
        return '--connect-now={address} --server-id={id}'.format(address=self._address, id=self._id)

class TeeWorldsServer(GameServer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, gameId='com.teeworlds.Teeworlds', **kwargs)

    def launchArgs(self):
        return 'teeworlds:{address}'.format(address=self._address)

class XonoticServer(GameServer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, gameId='org.xonotic.Xonotic', **kwargs)

    def launchArgs(self):
        return '+connect {address}'.format(address=self._address)

class OpenArenaServer(GameServer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, gameId='ws.openarena.OpenArena', **kwargs)

    def launchArgs(self):
        return '+connect {address}'.format(address=self._address)

class GameServerProviderMeta(type(QObject), ABCMeta):
    pass


class GameServerProvider(QObject, metaclass=GameServerProviderMeta):
    serversLoaded = pyqtSignal(object)

    def __init__(self, networkAccessManager=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._networkAccessManager = networkAccessManager
        self._threadpool = QThreadPool()
        self._reply = None

    @abstractmethod
    def load(self):
        pass

    def fromJson(self):
        return json.loads(bytearray(self._reply.readAll()).decode("utf-8"))

    def fromXml(self):
        doc = QDomDocument()
        doc.setContent(self._reply.readAll())
        return doc

    def processResult(self, output):
        servers = []
        for server in output:
            servers.append(
                getattr(sys.modules[__name__], self._className)(
                    address=server['address'],
                    players=server['players'],
                    maxPlayers=server['max_players'],
                    map=server['map'],
                    country=server['country'],
                    name=server['name'],
                )
            )

        self.serversLoaded.emit(servers)


class MinetestServerProvider(GameServerProvider):

    API_URI = 'https://servers.minetest.net/list'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._className = MinetestServer.__name__

    def load(self):
        worker = Worker(self.deserialize)
        worker.signals.result.connect(self.processResult)
        self._reply = self._networkAccessManager.get(QNetworkRequest(QUrl(self.API_URI)))
        self._reply.finished.connect(lambda: self._threadpool.start(worker))

    def deserialize(self):
        servers = self.fromJson()
        allServers = []
        for server in servers['list']:
            allServers.append({
                "address": server['address'] + ':' + str(server['port']),
                "players": str(server['clients']),
                "max_players": str(server['clients_max']),
                "map": server['mapgen'] if 'mapgen' in server else '',
                "country": server['geo_continent'] if 'geo_continent' in server else '',
                "name": server['name'] if 'name' in server else '',
            })

        return allServers


class OpenArenaServerProvider(GameServerProvider):

    API_URI = 'https://dpmaster.deathmask.net/?game=openarena&json=1'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._className = OpenArenaServer.__name__

    def load(self):
        worker = Worker(self.deserialize)
        worker.signals.result.connect(self.processResult)
        self._reply = self._networkAccessManager.get(QNetworkRequest(QUrl(self.API_URI)))
        self._reply.finished.connect(lambda: self._threadpool.start(worker))

    def deserialize(self):
        servers = self.fromJson()
        allServers = []
        for server in servers[1:]:
            allServers.append({
                "address": server['address'],
                "players": str(server['numplayers']) if 'numplayers' in server else '',
                "max_players": str(server['maxplayers']) if 'maxplayers' in server else '',
                "map": server['map'] if 'map' in server else '',
                "country": str(server['ping']) if 'ping' in server else '',
                "name": server['name'] if 'name' in server else '',
            })

        return allServers


class RedEclipseServerProvider(GameServerProvider):

    API_URI = 'https://redflare.ofthings.net/reports'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._className = RedEclipseServer.__name__

    def load(self):
        worker = Worker(self.deserialize)
        worker.signals.result.connect(self.processResult)
        self._reply = self._networkAccessManager.get(QNetworkRequest(QUrl(self.API_URI)))
        self._reply.finished.connect(lambda: self._threadpool.start(worker))

    def deserialize(self):
        servers = self.fromJson()
        allServers = []
        for address, server in servers.items():
            allServers.append({
                "address": address,
                "players": str(server['clients']),
                "max_players": str(server['maxClients']) if 'maxClients' in server else '',
                "map": server['mapName'],
                "country": server['country'] if 'country' in server else '',
                "name": server['description'] if 'description' in server else ''
            })

        return allServers

class SuperTuxKartServerProvider(GameServerProvider):

    API_URI = 'https://online.supertuxkart.net/api/v2/server/get-all'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def load(self):
        worker = Worker(self.deserialize)
        worker.signals.result.connect(self.processResult)
        self._reply = self._networkAccessManager.get(QNetworkRequest(QUrl(self.API_URI)))
        self._reply.finished.connect(lambda: self._threadpool.start(worker))

    def deserialize(self):
        root = self.fromXml()
        servers = root.elementsByTagName('server')
        allServers = []
        for index in range(servers.count()):
            server = servers.at(index).firstChildElement('server-info')
            allServers.append({
                "id": server.attribute('id'),
                "name": server.attribute('name'),
                "address": server.attribute('ip') + ':' + server.attribute('port'),
                "players": server.attribute('current_players'),
                "max_players": server.attribute('max_players'),
                "map": server.attribute('current_track'),
                "country": server.attribute('country_code'),
            })

        return allServers

    def processResult(self, output):
        servers = []
        for server in output:
            servers.append(
                SuperTuxKartServer(
                    id=server['id'],
                    address=server['address'],
                    players=server['players'],
                    maxPlayers=server['max_players'],
                    map=server['map'],
                    country=server['country'],
                    name=server['name']
                )
            )

        self.serversLoaded.emit(servers)

	
class TeeWorldsServerProvider(GameServerProvider):

    API_URI = 'https://api.status.tw/2.0/master/server/list/'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._className = TeeWorldsServer.__name__

    def load(self):
        worker = Worker(self.deserialize)
        worker.signals.result.connect(self.processResult)

        request = QNetworkRequest(QUrl(self.API_URI))
        request.setHeader(QNetworkRequest.ContentTypeHeader, 'application/x-www-form-urlencoded')

        body = QByteArray()
        body.append('master=master3.teeworlds.com')

        self._reply = self._networkAccessManager.post(request, body)
        self._reply.finished.connect(lambda: self._threadpool.start(worker))

    def deserialize(self):
        servers = self.fromJson()
        allServers = []
        for server in servers['servers']:
            allServers.append({
                "address": server['server_ip'] + ':' + str(server['server_port']),
                "players": str(server['num_players']) if 'num_players' in server else '',
                "max_players": str(server['max_players']) if 'max_players' in server else '',
                "map": server['map_name'] if 'map_name' in server else '',
                "country": str(server['ping']) if 'ping' in server else '',
                "name": server['name'] if 'name' in server else '',
            })

        return allServers

class XonoticServerProvider(GameServerProvider):

    API_URI = 'https://dpmaster.deathmask.net/?game=xonotic&json=1'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._className = XonoticServer.__name__

    def load(self):
        worker = Worker(self.deserialize)
        worker.signals.result.connect(self.processResult)
        self._reply = self._networkAccessManager.get(QNetworkRequest(QUrl(self.API_URI)))
        self._reply.finished.connect(lambda: self._threadpool.start(worker))

    def deserialize(self):
        servers = self.fromJson()
        allServers = []
        for server in servers[1:]:
            allServers.append({
                "address": server['address'],
                "players": str(server['numplayers']) if 'numplayers' in server else '',
                "max_players": str(server['maxplayers']) if 'maxplayers' in server else '',
                "map": server['map'] if 'map' in server else '',
                "country": str(server['ping']) if 'ping' in server else '',
                "name": server['name'] if 'name' in server else '',
            })

        return allServers

class ServerProvider(QObject):
    serversChanged = pyqtSignal()
    availableGamesChanged = pyqtSignal()

    def __init__(self, networkAccessManager=None, gameManager=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._networkAccessManager = networkAccessManager
        self._gameManager = gameManager
        self.loading = False
        self._showEmpty = True
        self._filterGame = None
        self._providers = {
            'net.minetest.Minetest': MinetestServerProvider(networkAccessManager=networkAccessManager),
            'ws.openarena.OpenArena': OpenArenaServerProvider(networkAccessManager=networkAccessManager),
            'net.redeclipse.RedEclipse': RedEclipseServerProvider(networkAccessManager=networkAccessManager),
            'net.supertuxkart.SuperTuxKart': SuperTuxKartServerProvider(networkAccessManager=networkAccessManager),
            'com.teeworlds.Teeworlds': TeeWorldsServerProvider(networkAccessManager=networkAccessManager),
            'org.xonotic.Xonotic': XonoticServerProvider(networkAccessManager=networkAccessManager)
        }
        self._servers = []
        self._availableGames = {}

        for gameId, gameServerProvider in self._providers.items():
            gameServerProvider.serversLoaded.connect(self.appendServers)

    def load(self):
        self._availableGames = {}
        for gameId in self._providers.keys():
            self._availableGames[gameId] = self._gameManager.getGameById(gameId)
        self.availableGamesChanged.emit()            

    def appendServers(self, servers):
        self._servers = self._servers + servers
        self.serversChanged.emit()

    @pyqtSlot()
    def refresh(self):
        self._servers = []
        for gameId, gameServerProvider in self._providers.items():
            gameServerProvider.load()
    
    @pyqtProperty(QQmlListProperty, notify=serversChanged)
    def servers(self):
        servers = self._servers
        if (self.filterGame):
            servers = list(filter(lambda server: server.gameId == self.filterGame, servers))

        if (not self.showEmpty):
            servers = list(filter(lambda server: server.players != "0", servers))
            

        return QQmlListProperty(GameServer, self, servers)

    @pyqtProperty('QVariant', notify=availableGamesChanged)
    def availableGames(self):
        return self._availableGames

    @pyqtSlot(str, result='QJsonObject')
    def getGameDetailsForId(self, gameId):
        game = self._gameManager.getGameById(gameId)
        return {
            'id': game.id,
            'name': game.name,
            'icon': game.iconSmall
        }

    @pyqtProperty(bool, notify=serversChanged)
    def showEmpty(self):
        return self._showEmpty

    @showEmpty.setter
    def showEmpty(self, showEmpty):
        if showEmpty != self._showEmpty:
            self._showEmpty = showEmpty
            self.serversChanged.emit()
    
    @pyqtProperty('QString', notify=serversChanged)
    def filterGame(self):
        return self._filterGame

    @filterGame.setter
    def filterGame(self, filterGame):
        if filterGame != self._filterGame:
            self._filterGame = filterGame
            self.serversChanged.emit()