<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ca_ES">
<context>
    <name>GameView</name>
    <message>
        <source>Install</source>
        <translation type="obsolete">Instal·lar</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="obsolete">Sí</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="obsolete">Cancel·lar</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="obsolete">Jugar</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="obsolete">Descripcció</translation>
    </message>
    <message>
        <source>Releases</source>
        <translation type="obsolete">Publicacions</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation type="obsolete">Versió %1</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="obsolete">Desenvolupador</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="obsolete">Llicència</translation>
    </message>
    <message>
        <source>Links</source>
        <translation type="obsolete">Enllaços</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="obsolete">Pàgina principal</translation>
    </message>
    <message>
        <source>Bug Tracker</source>
        <translation type="obsolete">Seguidor d&apos;errades</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="obsolete">Ajuda</translation>
    </message>
    <message>
        <source>FAQ</source>
        <translation type="obsolete">FAQ</translation>
    </message>
    <message>
        <source>Donate</source>
        <translation type="obsolete">Fer un donatiu</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation type="obsolete">Traducció</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="obsolete">Desconegut</translation>
    </message>
    <message>
        <source>Manifest</source>
        <translation type="obsolete">Manifest</translation>
    </message>
</context>
<context>
    <name>LibraryGridView</name>
    <message>
        <location filename="../LibraryGridView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Cerca %L1 jocs...</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="82"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="83"/>
        <source>Installed (%L1)</source>
        <translation type="unfinished">Instal·lats</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="84"/>
        <source>Recent (%L1)</source>
        <translation type="unfinished">Recents</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="85"/>
        <source>Has Updates (%L1)</source>
        <translation type="unfinished">Conté actualitzacions</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="86"/>
        <source>Processing (%L1)</source>
        <translation type="unfinished">Processant</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="167"/>
        <source>Play</source>
        <translation type="unfinished">Jugar</translation>
    </message>
    <message>
        <location filename="../LibraryGridView.qml" line="176"/>
        <source>Install</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryListView</name>
    <message>
        <location filename="../LibraryListView.qml" line="24"/>
        <source>Search %L1 Games...</source>
        <translation type="unfinished">Cerca %L1 jocs...</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="108"/>
        <source>All (%L1)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="109"/>
        <source>Installed (%L1)</source>
        <translation type="unfinished">Instal·lats</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="110"/>
        <source>Recent (%L1)</source>
        <translation type="unfinished">Recents</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="111"/>
        <source>Has Updates (%L1)</source>
        <translation type="unfinished">Conté actualitzacions</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="112"/>
        <source>Processing (%L1)</source>
        <translation type="unfinished">Processant</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="299"/>
        <source>New</source>
        <translation type="unfinished">Nou</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="319"/>
        <source>Nothing seems to be here.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="424"/>
        <source>Install</source>
        <translation type="unfinished">Instal·lar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="439"/>
        <source>Download Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="446"/>
        <source>Installed Size:	%1</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="453"/>
        <source>Install Game?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="484"/>
        <source>Play</source>
        <translation type="unfinished">Jugar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="484"/>
        <source>In-Game</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="504"/>
        <source>Update</source>
        <translation type="unfinished">Actualitzar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="507"/>
        <source>Uninstall</source>
        <translation type="unfinished">Desinstal·lar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="528"/>
        <source>Are you sure?</source>
        <translation type="unfinished">Estàs segur?</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="778"/>
        <source>Description</source>
        <translation type="unfinished">Descripcció</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="783"/>
        <source>No description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="804"/>
        <source>Similar Games</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="463"/>
        <location filename="../LibraryListView.qml" line="538"/>
        <source>Yes</source>
        <translation type="unfinished">Sí</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="151"/>
        <source>Action</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="152"/>
        <source>Adventure</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="153"/>
        <source>Arcade</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="154"/>
        <source>Board</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="155"/>
        <source>Blocks</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="156"/>
        <source>Card</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="157"/>
        <source>Kids</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="158"/>
        <source>Logic</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="159"/>
        <source>RolePlaying</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="160"/>
        <source>Shooter</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="161"/>
        <source>Simulation</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="162"/>
        <source>Sports</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="163"/>
        <source>Strategy</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="180"/>
        <source>Apply</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="189"/>
        <source>Clear</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="466"/>
        <location filename="../LibraryListView.qml" line="541"/>
        <location filename="../LibraryListView.qml" line="556"/>
        <location filename="../LibraryListView.qml" line="610"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancel·lar</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="495"/>
        <source>Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="577"/>
        <source>Resolve Error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="589"/>
        <source>Clear error</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="594"/>
        <source>Mark as installed</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="601"/>
        <source>Mark as uninstalled</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="832"/>
        <source>Reviews</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="837"/>
        <source>This game has no reviews.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1008"/>
        <source>Releases</source>
        <translation type="unfinished">Publicacions</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1013"/>
        <source>No release information available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1036"/>
        <source>Version %1</source>
        <translation type="unfinished">Versió %1</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1051"/>
        <source>No release description available.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1068"/>
        <source>Anti-Features</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1081"/>
        <source>This game requires NonFree assets.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1083"/>
        <source>This game requires NonFree network services.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1096"/>
        <source>Developer</source>
        <translation type="unfinished">Desenvolupador</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1110"/>
        <source>License</source>
        <translation type="unfinished">Llicència</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1139"/>
        <source>Links</source>
        <translation type="unfinished">Enllaços</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1169"/>
        <source>Homepage</source>
        <translation type="unfinished">Pàgina principal</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1171"/>
        <source>Bug Tracker</source>
        <translation type="unfinished">Seguidor d&apos;errades</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1173"/>
        <source>Help</source>
        <translation type="unfinished">Ajuda</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1175"/>
        <source>FAQ</source>
        <translation type="unfinished">FAQ</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1178"/>
        <source>Donate</source>
        <translation type="unfinished">Fer un donatiu</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1180"/>
        <source>Translation</source>
        <translation type="unfinished">Traducció</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1182"/>
        <source>Unknown</source>
        <translation type="unfinished">Desconegut</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1184"/>
        <source>Manifest</source>
        <translation type="unfinished">Manifest</translation>
    </message>
    <message>
        <location filename="../LibraryListView.qml" line="1186"/>
        <source>Contact</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>LibraryView</name>
    <message>
        <source>Search %L1 Games...</source>
        <translation type="vanished">Cerca %L1 jocs...</translation>
    </message>
    <message>
        <source>Library</source>
        <translation type="vanished">Biblioteca</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Paràmetres</translation>
    </message>
    <message>
        <source>Check For Updates</source>
        <translatorcomment>Shortened the translation a bit so that it fits</translatorcomment>
        <translation type="vanished">Comprova actualitzacions</translation>
    </message>
    <message>
        <source>Update All</source>
        <translation type="vanished">Actualitza-ho tot</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Surt</translation>
    </message>
    <message>
        <source>You have operations pending.</source>
        <translation type="vanished">Tens operacions pendents.</translation>
    </message>
    <message>
        <source>Close Anyway</source>
        <translation type="vanished">Tanca igualment</translation>
    </message>
    <message>
        <source>All Games (%L1)</source>
        <translation type="vanished">Tots els jocs (%L1)</translation>
    </message>
    <message>
        <source>Installed (%L1)</source>
        <translation type="vanished">Instal·lats (%L1)</translation>
    </message>
    <message>
        <source>Recent (%L1)</source>
        <translation type="vanished">Recents (%L1)</translation>
    </message>
    <message>
        <source>New (%L1)</source>
        <translation type="vanished">Nous (%L1)</translation>
    </message>
    <message>
        <source>Has Updates (%L1)</source>
        <translation type="vanished">Amb actualitzacions (%L1)</translation>
    </message>
    <message>
        <source>Processing (%L1)</source>
        <translation type="vanished">Processant (%L1)</translation>
    </message>
    <message>
        <source>New</source>
        <translation type="vanished">Nou</translation>
    </message>
    <message>
        <source>Install</source>
        <translation type="vanished">Instal·la</translation>
    </message>
    <message>
        <source>Play</source>
        <translation type="vanished">Juga-hi</translation>
    </message>
    <message>
        <source>Update</source>
        <translation type="vanished">Actualitza</translation>
    </message>
    <message>
        <source>Uninstall</source>
        <translation type="vanished">Desinstal·la</translation>
    </message>
    <message>
        <source>Are you sure?</source>
        <translation type="vanished">Estàs segur?</translation>
    </message>
    <message>
        <source>Yes</source>
        <translation type="vanished">Sí</translation>
    </message>
    <message>
        <source>Cancel</source>
        <translation type="vanished">Cancel·la</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="vanished">Descripció</translation>
    </message>
    <message>
        <source>Releases</source>
        <translation type="vanished">Llançaments</translation>
    </message>
    <message>
        <source>Version %1</source>
        <translation type="vanished">Versió %1</translation>
    </message>
    <message>
        <source>Developer</source>
        <translation type="vanished">Desenvolupador</translation>
    </message>
    <message>
        <source>License</source>
        <translation type="vanished">Llicència</translation>
    </message>
    <message>
        <source>Links</source>
        <translation type="vanished">Enllaços</translation>
    </message>
    <message>
        <source>Homepage</source>
        <translation type="vanished">Pàgina principal</translation>
    </message>
    <message>
        <source>Bug Tracker</source>
        <translation type="vanished">Seguidor d&apos;errades</translation>
    </message>
    <message>
        <source>Help</source>
        <translation type="vanished">Ajuda</translation>
    </message>
    <message>
        <source>FAQ</source>
        <translation type="vanished">PMF</translation>
    </message>
    <message>
        <source>Donate</source>
        <translation type="vanished">Fes un donatiu</translation>
    </message>
    <message>
        <source>Translation</source>
        <translation type="vanished">Traducció</translation>
    </message>
    <message>
        <source>Unknown</source>
        <translation type="vanished">Desconegut</translation>
    </message>
    <message>
        <source>Manifest</source>
        <translation type="vanished">Manifest</translation>
    </message>
    <message>
        <source>Categories</source>
        <translation type="vanished">Categories</translation>
    </message>
    <message>
        <source>Installed successfully.</source>
        <translation type="vanished">S&apos;ha instal·lat correctament.</translation>
    </message>
    <message>
        <source>Uninstalled successfully.</source>
        <translation type="vanished">S&apos;ha desinstal·lat correctament.</translation>
    </message>
    <message>
        <source>Updated successfully.</source>
        <translation type="vanished">S&apos;ha actualitzat correctament.</translation>
    </message>
    <message>
        <source>An error occurred.</source>
        <translation type="vanished">S&apos;ha produït un error.</translation>
    </message>
</context>
<context>
    <name>NavigationBar</name>
    <message>
        <location filename="../NavigationBar.qml" line="18"/>
        <source>Library</source>
        <translation type="unfinished">Biblioteca</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="29"/>
        <source>Servers</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="40"/>
        <source>Settings</source>
        <translation type="unfinished">Paràmetres</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="61"/>
        <source>Show games in a list view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="71"/>
        <source>Show games in a grid view.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="80"/>
        <source>Reset settings to defaults.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="92"/>
        <source>Check For Updates</source>
        <translation type="unfinished">Comprova actualitzacions</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="96"/>
        <source>Update All</source>
        <translation type="unfinished">Actualitza-ho tot</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="100"/>
        <source>Exit</source>
        <translation type="unfinished">Sortir</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="115"/>
        <source>You have operations pending.</source>
        <translation type="unfinished">Tens operacions pendents.</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="121"/>
        <source>Close Anyway</source>
        <translation type="unfinished">Tanca igualment</translation>
    </message>
    <message>
        <location filename="../NavigationBar.qml" line="127"/>
        <source>Cancel</source>
        <translation type="unfinished">Cancel·lar</translation>
    </message>
</context>
<context>
    <name>ServersView</name>
    <message>
        <location filename="../ServersView.qml" line="66"/>
        <location filename="../ServersView.qml" line="72"/>
        <location filename="../ServersView.qml" line="89"/>
        <location filename="../ServersView.qml" line="95"/>
        <source>?</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="174"/>
        <source>All</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="182"/>
        <source>Show empty</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <location filename="../ServersView.qml" line="190"/>
        <source>%L1 servers found.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>SettingsView</name>
    <message>
        <source>‹</source>
        <translation type="vanished">‹</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation type="vanished">Paràmetres</translation>
    </message>
    <message>
        <source>⋮</source>
        <translation type="vanished">⋮</translation>
    </message>
    <message>
        <source>Reset All</source>
        <translation type="vanished">Restableix-ho tot</translation>
    </message>
    <message>
        <source>Exit</source>
        <translation type="vanished">Surt</translation>
    </message>
    <message>
        <source>Show Tray Icon</source>
        <translation type="vanished">Mostra una icona a la safata del sistema</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="28"/>
        <source>Always Show Logs</source>
        <translation>Mostra sempre els registres</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="35"/>
        <source>Notifications Enabled</source>
        <translation>Notificacions habilitades</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="46"/>
        <source>Theme</source>
        <translation>Tema</translation>
    </message>
    <message>
        <location filename="../SettingsView.qml" line="69"/>
        <source>Reset Database</source>
        <translation>Reinicialitza la base de dades</translation>
    </message>
</context>
</TS>
